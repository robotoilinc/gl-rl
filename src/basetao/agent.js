import { STATES, Storage } from '../helpers/storage';
import { Snackbar } from '../helpers/snackbar';
import BaseTaoElement from './element';

export default class BaseTao {
  constructor() {
    // Ensure the toast looks decent on Basetao
    GM_addStyle('.swal2-popup.swal2-toast .swal2-title {font-size: 1.5em; font-weight: bolder}');

    // Get the username
    const username = $('#dropdownMenu1').text();
    if (typeof username === 'undefined' || username == null || username === '') {
      Snackbar('You need to be logged in to use this extension.');

      return this;
    }

    this.storage = new Storage();

    return this;
  }

  /**
   * @return {Promise<void>}
   */
  async process() {
    // Make copy of the current this, so we can use it later
    const agent = this;

    // Get the container
    const $container = $('.myparcels-ul').first();

    // Add icons to all elements
    $container.find('.tr-bodnone').each(function () {
      agent._buildElement($(this));
    });
  }

  /**
   * @private
   * @param $this
   * @return {Promise<BaseTaoElement>}
   */
  async _buildElement($this) {
    const element = new BaseTaoElement($this);
    const agent = this;

    const $marker = $('<ul class="gl-rl" style="min-width: 4em;padding: 0;border: 1px black dotted;"><li style="width: 100%;text-align: center;"></li></ul>');
    $this.find('td:nth-child(6)').append($marker);

    const $gl = $('<span style="width: 49%;padding: 2px;font-weight: bold;" title="Green light">✔️</span>');
    const $rl = $('<span style="width: 49%;padding: 2px;font-weight: bold;" title="Red light">🛑</span>');
    const $swap = $('<span style="width: 49%;cursor: pointer;padding: 2px;font-weight: bold;" title="Swap">🔁</span>');

    // Always assign the click to swap, because it doesn't matter
    $swap.on('click', () => {
      if (this.storage.get(element.orderId) === STATES.GREEN_LIGHT) {
        this.storage.set(element.orderId, STATES.RED_LIGHT);
        Snackbar('Swapped to RL!');

        // Remove and rebuild
        $marker.remove();
        agent._buildElement($this);

        return;
      }

      if (this.storage.get(element.orderId) === STATES.RED_LIGHT) {
        this.storage.set(element.orderId, STATES.GREEN_LIGHT);
        Snackbar('Swapped to GL!');

        // Remove and rebuild
        $marker.remove();
        agent._buildElement($this);
      }
    });

    // If it already exists, show swap or gl/rl without pointers
    if (this.storage.exists(element.orderId)) {
      if (this.storage.get(element.orderId) === STATES.GREEN_LIGHT) {
        $marker.find('li').append($gl);
      }

      if (this.storage.get(element.orderId) === STATES.RED_LIGHT) {
        $marker.find('li').append($rl);
      }

      $marker.find('li').append($swap);

      return element;
    }

    // Make the spans pointers
    $gl.css('cursor', 'pointer');
    $rl.css('cursor', 'pointer');

    $gl.on('click', () => {
      this.storage.set(element.orderId, STATES.GREEN_LIGHT);
      Snackbar('GL, good stuff!');

      // Remove and rebuild
      $marker.remove();
      agent._buildElement($this);
    });

    $rl.on('click', () => {
      this.storage.set(element.orderId, STATES.RED_LIGHT);
      Snackbar('RL? Sucks to suck');

      // Remove and rebuild
      $marker.remove();
      agent._buildElement($this);
    });

    $marker.find('li').append($gl, $rl);

    return element;
  }
}
