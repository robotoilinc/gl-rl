export default class BaseTaoElement {
  constructor($element) {
    this.object = $element;

    // eslint-disable-next-line prefer-destructuring
    this.orderId = $element.find('td:nth-child(7) > ul:nth-child(3) > li > a').first().attr('href').trim().match(/itemimg\/(\d+)\.html/)[1];
  }
}
