export const STATES = Object.freeze({
  GREEN_LIGHT: 'green_light',
  RED_LIGHT: 'red_light',
});

export class Storage {
  get(orderId) {
    return GM_getValue(orderId, null);
  }

  exists(orderId) {
    return this.get(orderId) !== null;
  }

  set(orderId, state) {
    if (!Object.values(STATES).includes(state)) {
      throw new Error('Invalid state has been passed');
    }

    return GM_setValue(orderId, state);
  }
}
