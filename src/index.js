import { Snackbar } from './helpers/snackbar';
import BaseTao from './basetao/agent';

// Inject snackbar css style
GM_addStyle(GM_getResourceText('sweetalert2'));

// eslint-disable-next-line func-names
(async function () {
  // Setup the logger.
  Logger.useDefaults();

  // Log the start of the script.
  Logger.info(`Starting extension '${GM_info.script.name}', version ${GM_info.script.version}`);

  // Start building the GL/RL list
  try {
    await new BaseTao().process();
  } catch (error) {
    Snackbar(`An unknown issue has occurred when trying to setup the extension: ${error.message}`);
    Logger.error('An unknown issue has occurred', error);
  }
}());
